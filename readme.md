# Mentalh-Health-Webserver

## Description
This is the web server for mental health platform. Web server will provide API and data for both the mobile application and web application.

## Getting Started
- Make sure that `node` and `npm` is installed on your mac / windows / linux
- `npm install`
- `node App.js`, you can also use `nodemon` if you have installed it globally

## Testing
We use `Chai` and `Mocha` for testing. Commands and instructions will follow after finishing the web server.

## Database Schema
Overall, here is the schema used by mental health platform:
- Doctor Schema
	- Fields: `email`, `username`, `password`, `passwordConf`, `gender`, `hospital`
- Patient Schema
	- Fields: `email`, `username`, `password`, `passwordConf`, `gender`, `hospital`, `condition`
- Daily emotion Schema
	- Fields: `dailyEmotionId`, `patientEmail`, `timestamp`, `emotion`
- Shape Activities Schema
	- Fields: `shapeActivityId`, `patientEmail`, `timestamp`, `image` 
- Color Activities Schema
	- Fields: `colorActivityId`, `patientEmail`, `timestamp`, `colorChoice`
- Picture Activities Schema
	- Fields: `pictureActivityId`, `patientEmail`, `timestamp`, `image`
- Drawing Activities Schema
	- Fields: `drawingActivityId`, `patientEmail`, `timestamp`, `image`
- Questionnaire Question Schema
	- Fields: `questionnaireQuestionId`, `questionnaireId`, `question`, `questionIndex`, `format`, `firstChoice`, `secondChoice`, `thirdChoice`, `fourthChoice`, `fifthChoice`, `textAns`
- QuestionnaireAnswer Schema
	- Fields: `questionnaireAnswerId`, `questionnaireId`, `patientEmail`, `timestamp`, `answers`
- Doctor Subscription Schema
	- Fields: `doctorSubscriptionId`, `doctorEmail`, `patientEmail`
- Caching Doctor Schema
	- Fields: `cachingDoctorId`, `doctorEmail`, `patientEmail`, `timestamp`, `activityName`
- Caching Patient Schema
	- Fields: `cachingPatientId`, `patientEmail`, `timestamp`, `activityName`

For the schema above, there are always `itemId` to identify unique item for any user on any particular timestamp.
`itemId` is constructed using this format: `activityName`:`timestamp`:`userId`
`questionnaireAnswerId` is constructed using this format: `questionnaireId` + `counter`

For the doctor and patient schema, it won't have `itemId` and will be identified by `email`.
All other fields will be stored as `String` or `binData`. `String` will be used for `Daily Emotion`, `Color Activities`, 
`Questionnaire`, and `QuestionnaireAnswer`.

Subscription schema's `Id` is constructed by `doctorEmail`:`patientEmail`

Caching schema's `Id` construction will be done by {`doctorEmail`|`patientEmail`}:`timestamp`

Additional field can be added freely, considering that we use NoSQL schema, so backward compability is always guaranteed.

## API
API will be provided to do three basic things:
- Get item
- Add item
- Delete item

For the cases above, please take note that item can be gotten by `itemId` or `userId`

## TODO
- [ ] Limit each entry for caching layer for doctors and patients to just be 5 and 1
- [ ] Change from email to username
- [ ] Forgot Password API
- [ ] Modify Patient Details and Create Questionnaires to use API
- [ ] Configure ESLint and Prettier for better code style
- [ ] Add unit test for API

## Contact
For questions and inqueries about this repository, please email `andreveeguil@gmail.com`
