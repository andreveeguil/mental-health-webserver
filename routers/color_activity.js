var express = require('express');
var router = express.Router();
var ColourActivity = require('../models/colour_activity');

// Get Colour Activity by id
router.get('/:colourActivityId', function(req, res){
    colourActivityId = req.params.colourActivityId;

    ColourActivity.find({ colourActivityId : colourActivityId}, function(err, colourActivity) {
        res.send("Colour Activity gotten " + colourActivity)
    });
});

// Get Colour Activitys by patient email
router.get('/email/:patientEmail', function(req, res) {
    patientEmail = req.params.patientEmail;

    ColourActivity.find({ patientEmail: patientEmail}, function(err, colourActivities) {
        res.send("Colour Activities gotten " + colourActivities);
    })
})

// Create new Colour Activity data
router.post('/', function(req, res){
    if (req.body.colourActivityId && req.body.patientEmail && req.body.timestamp && req.body.colourChoice){
        var newColourActivity = new ColourActivity(req.body);
        newColourActivity.save(function(err, newColourActivity){
            if (err) return console.error(err);
            res.send('Colour Activity has been submitted for the day ' + newColourActivity);
        })
    }
});

// Delete Colour Activity data
router.delete('/:colourActivityId', function(req, res){
    ColourActivity.deleteOne({
        colourActivityId: req.params.colourActivityId
    }, function(err, colourActivity){});
    res.send('Deleting is complete for ' + req.params.colourActivityId);
});

module.exports = router
