var express = require('express');
var router = express.Router();
var CachingPatient = require('../models/caching_patient');

// Get Caching Patient by id
router.get('/:cachingPatientId', function(req, res){
    cachingPatientId = req.params.cachingPatientId;

    CachingPatient.find({ cachingPatientId : cachingPatientId}, function(err, cachingPatient) {
        res.send("Caching Patient gotten " + cachingPatient)
    });
});

// Get Caching Patients by patient's email
router.get('/email/:patientEmail', function(req, res) {
    patientEmail = req.params.patientEmail;

    CachingPatient.find({ patientEmail: patientEmail}, function(err, cachingPatients) {
        res.send("Caching Patients gotten " + cachingPatients);
    })
})


// Create new Caching Patient data
router.post('/', function(req, res){
	if (req.body.cachingPatientId && req.body.patientEmail && 
		req.body.timestamp && req.body.activityName)
	{
        var newCachingPatient = new CachingPatient(req.body);
        newCachingPatient.save(function(err, newCachingPatient){
            if (err) return console.error(err);
            res.send('Caching Patient has been submitted for the day ' + newCachingPatient);
        })
    }
});

// Delete Caching Patient data
router.delete('/:cachingPatientId', function(req, res){
    CachingPatient.deleteOne({
        cachingPatientId: req.params.cachingPatientId
    }, function(err, cachingPatient){});
    res.send('Deleting is complete for ' + req.params.cachingPatientId);
});

module.exports = router
