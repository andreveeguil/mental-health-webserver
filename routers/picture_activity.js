var express = require('express');
var router = express.Router();
var PictureActivity = require('../models/picture_activity');

// Get Picture Activity by id
router.get('/:pictureActivityId', function(req, res){
    pictureActivityId = req.params.pictureActivityId;

    PictureActivity.find({ pictureActivityId : pictureActivityId}, function(err, pictureActivity) {
        res.send("Picture Activity gotten " + pictureActivity)
    });
});

// Get Picture Activities by patient email
router.get('/email/:patientEmail', function(req, res) {
    patientEmail = req.params.patientEmail;

    PictureActivity.find({ patientEmail: patientEmail}, function(err, colourActivities) {
        res.send("Colour Activities gotten " + colourActivities);
    })
})

// Create new Picture Activity data
router.post('/', function(req, res){
    if (req.body.pictureActivityId && req.body.patientEmail && req.body.timestamp && req.body.image){
        var newpictureActivity = new PictureActivity(req.body);
        newpictureActivity.save(function(err, newpictureActivity){
            if (err) return console.error(err);
            res.send('Picture Activity has been submitted for the day ' + newpictureActivity);
        })
    }
});

// Delete Picture Activity data
router.delete('/:pictureActivityId', function(req, res){
    PictureActivity.deleteOne({
        pictureActivityId: req.params.pictureActivityId
    }, function(err, pictureActivity){});
    res.send('Deleting is complete for ' + req.params.pictureActivityId);
});

module.exports = router
