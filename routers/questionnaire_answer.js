var express = require('express');
var router = express.Router();
var QuestionnaireAnswer = require('../models/questionnaire_answer');

// Get Questionnaire Answer by id
router.get('/:questionnaireAnswerId', function(req, res){
    questionnaireAnswerId = req.params.questionnaireAnswerId;

    QuestionnaireAnswer.find({ questionnaireAnswerId : questionnaireAnswerId}, function(err, questionnaireAnswer) {
        res.send("Questionnaire Answer gotten " + questionnaireAnswer)
    });
});

// Get Questionnaire Answers by questionnaire Id
router.get('/questionnaire/:questionnaireId', function(req, res) {
    questionnaireId = req.params.questionnaireId;

    QuestionnaireAnswer.find({ questionnaireId: questionnaireId}, function(err, questionnaireAnswers) {
        res.send("Questionnaire Answers gotten " + questionnaireAnswers);
    })
})

// Create new Questionnaire Answer data
router.post('/', function(req, res){
    if (req.body.questionnaireAnswerId && req.body.questionnaireId && req.body.timestamp && req.body.timestamp){
        var newQuestionnaireAnswer = new QuestionnaireAnswer(req.body);
        newQuestionnaireAnswer.save(function(err, newQuestionnaireAnswer){
            if (err) return console.error(err);
            res.send('Questionnaire Answer has been submitted for the day ' + newQuestionnaireAnswer);
        })
    }
});

// Delete Questionnaire Answer data
router.delete('/:questionnaireAnswerId', function(req, res){
    QuestionnaireAnswer.deleteOne({
        questionnaireAnswerId: req.params.questionnaireAnswerId
    }, function(err, questionnaireAnswer){});
    res.send('Deleting is complete for ' + req.params.questionnaireAnswerId);
});

module.exports = router
