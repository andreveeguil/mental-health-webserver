var express = require('express');
var router = express.Router();
var ShapeActivity = require('../models/shape_activity');

// Get Shape Activity by id
router.get('/:shapeActivityId', function(req, res){
    shapeActivityId = req.params.shapeActivityId;

    ShapeActivity.find({ shapeActivityId : shapeActivityId}, function(err, shapeActivity) {
        res.send("Shape Activity gotten " + shapeActivity)
    });
});

// Get Shape Activities by patient email
router.get('/email/:patientEmail', function(req, res) {
    patientEmail = req.params.patientEmail;

    ShapeActivity.find({ patientEmail: patientEmail}, function(err, shapeActivities) {
        res.send("Shape Activities gotten " + shapeActivities);
    })
})

// Create new Shape Activity data
router.post('/', function(req, res){
    if (req.body.shapeActivityId && req.body.patientEmail && req.body.timestamp && req.body.image){
        var newShapeActivity = new ShapeActivity(req.body);
        newShapeActivity.save(function(err, newShapeActivity){
            if (err) return console.error(err);
            res.send('Shape Activity has been submitted for the day ' + newShapeActivity);
        })
    }
});

// Delete Shape Activity data
router.delete('/:shapeActivityId', function(req, res){
    ShapeActivity.deleteOne({
        shapeActivityId: req.params.shapeActivityId
    }, function(err, shapeActivity){});
    res.send('Deleting is complete for ' + req.params.shapeActivityId);
});

module.exports = router
