var express = require('express');
var router = express.Router();
var DoctorUser = require('../models/doctor_user');

// Register end point
router.post('/register', function(req, res, next){
    if (req.body.password !== req.body.passwordConf){
        var err = new Error('Password do not match');
        err.status = 400;
        res.send("password dont match");
        return next(err);
    }

    if (
        req.body.email && req.body.username && req.body.password &&
        req.body.passwordConf && req.body.gender && req.body.hospital
    ){
        var doctorData = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
            passwordConf: req.body.passwordConf,
            gender: req.body.gender,
            hospital: req.body.hospital
        }
        DoctorUser.create(doctorData, function(error, doctorUser){
            if (error){
                return next(error);
            }
            else {
                req.session.userId = doctorUser._id;
                res.send("Doctor successfully created");
            }
        })
    }
    else {
        var err = new Error('All fields required for registration');
        err.status = 400;
        return next(err);
    }
})

// Login end point
router.post('/login', function(req, res, next){
    if (req.body.logemail && req.body.logpassword) {
        DoctorUser.authenticate(req.body.logemail, req.body.logpassword, function(error, doctorUser) {
            if (error || !doctorUser){
                var err = new Error('Wrong email or password');
                err.status = 401;
                return next(err);
            } else {
                req.session.userId = doctorUser._id;
                res.send("Doctor successfully login");
            }
        })
    }
    else {
        var err = new Error('Email and password required for login');
        err.status = 400;
        return next(err);
    }
})

router.post('/updateParticular', function(req, res, next){
    var matchEmail = {
        'email': req.body.email
    }
    DoctorUser.findOneAndUpdate(matchEmail, req.body, function(err, doc) {
        if (err) return res.send(500, {error: err});
        return res.send("Successfully saved");
    })
})

// Logout end point
router.get('/logout', function(req, res, next){
    if (req.session){
        req.session.destroy(function(err) {
            if (err) {
                return next(err);
            }
            else {
                res.send("Successfully logout.")
            }
        })
    }
})

// Delete end point
router.delete('/delete', function(req, res){
    DoctorUser.remove({
        email: req.body.email
    }, function(err, doctorUser){});
    res.send('Deleting is complete for ' + req.body.email);
});

module.exports = router
