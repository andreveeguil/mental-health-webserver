var express = require('express');
var router = express.Router();
var QuestionnaireQuestion = require('../models/questionnaire_question');

// Get Questionnaire Question by id
router.get('/:questionnaireQuestionId', function(req, res){
    questionnaireQuestionId = req.params.questionnaireQuestionId;

    QuestionnaireQuestion.find({ questionnaireQuestionId : questionnaireQuestionId}, function(err, questionnaireQuestion) {
        res.send("Questionnaire Question gotten " + questionnaireQuestion)
    });
});

// Get Questionnaire Questions by questionnaire Id
router.get('/questionnaire/:questionnaireId', function(req, res) {
    questionnaireId = req.params.questionnaireId;

    QuestionnaireQuestion.find({ questionnaireId: questionnaireId}, function(err, questionnaireQuestions) {
        res.send("Questionnaire Questions gotten " + questionnaireQuestions);
    })
})

// Create new Questionnaire Question data
router.post('/', function(req, res){
    if (req.body.questionnaireQuestionId && req.body.questionnaireId && req.body.question && req.body.format){
        var newQuestionnaireQuestion = new QuestionnaireQuestion(req.body);
        newQuestionnaireQuestion.save(function(err, newQuestionnaireQuestion){
            if (err) return console.error(err);
            res.send('Questionnaire Question has been submitted for the day ' + newQuestionnaireQuestion);
        })
    }
});

// Delete Questionnaire Question data
router.delete('/:questionnaireQuestionId', function(req, res){
    QuestionnaireQuestion.deleteOne({
        questionnaireQuestionId: req.params.questionnaireQuestionId
    }, function(err, questionnaireQuestion){});
    res.send('Deleting is complete for ' + req.params.questionnaireQuestionId);
});

module.exports = router
