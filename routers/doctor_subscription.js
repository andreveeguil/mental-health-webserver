var express = require('express');
var router = express.Router();
var DoctorSubscription = require('../models/doctor_subscription');

// Get Doctor Subscription by id
router.get('/:doctorSubscriptionId', function(req, res){
    doctorSubscriptionId = req.params.doctorSubscriptionId;

    DoctorSubscription.find({ doctorSubscriptionId : doctorSubscriptionId}, function(err, doctorSubscription) {
        res.send("Doctor Subscription gotten " + doctorSubscription)
    });
});

// Get Doctor Subscriptions by doctor's email
router.get('/email/:doctorEmail', function(req, res) {
    doctorEmail = req.params.doctorEmail;

    DoctorSubscription.find({ doctorEmail: doctorEmail}, function(err, doctorSubscriptions) {
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(doctorSubscriptions));
    })
})

// Create new Doctor Subscription data
router.post('/', function(req, res){
    if (req.body.doctorSubscriptionId && req.body.doctorEmail && req.body.patientEmail){
        var newDoctorSubscription = new DoctorSubscription(req.body);
        newDoctorSubscription.save(function(err, newDoctorSubscription){
            if (err) return console.error(err);
            res.send('Doctor Subscription has been submitted for the day ' + newDoctorSubscription);
        })
    }
});

// Delete Doctor Subscription data
router.delete('/:doctorSubscriptionId', function(req, res){
    DoctorSubscription.deleteOne({
        doctorSubscriptionId: req.params.doctorSubscriptionId
    }, function(err, doctorSubscription){});
    res.send('Deleting is complete for ' + req.params.doctorSubscriptionId);
});

module.exports = router
