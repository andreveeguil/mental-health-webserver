var express = require('express');
var router = express.Router();
var DailyEmotion = require('../models/daily_emotion');

// Get daily emotion by id
router.get('/:dailyEmotionId', function(req, res){
    dailyEmotionId = req.params.dailyEmotionId;

    DailyEmotion.find({ dailyEmotionId : dailyEmotionId}, function(err, dailyEmotion) {
        res.send("Daily emotion gotten " + dailyEmotion)
    });
});

// Get daily emotions by patient email
router.get('/email/:patientEmail', function(req, res) {
    patientEmail = req.params.patientEmail;

    DailyEmotion.find({ patientEmail: patientEmail}, function(err, dailyEmotions) {
        res.send("Daily emotions gotten " + dailyEmotions);
    })
})

// Create new daily emotion data
router.post('/', function(req, res){
    if (req.body.dailyEmotionId && req.body.patientEmail && req.body.timestamp && req.body.emotion){
        var newDailyEmotion = new DailyEmotion(req.body);
        newDailyEmotion.save(function(err, newDailyEmotion){
            if (err) return console.error(err);
            res.send('Daily Emotion has been submitted for the day');
        })
    }
});

// Delete daily emotion data
router.delete('/:dailyEmotionId', function(req, res){
    DailyEmotion.deleteOne({
        dailyEmotionId: req.params.dailyEmotionId
    }, function(err, patient){});
    res.send('Deleting is complete for ' + req.params.dailyEmotionId);
});

module.exports = router
