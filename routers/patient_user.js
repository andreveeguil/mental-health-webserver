var express = require('express');
var router = express.Router();
var PatientUser = require('../models/patient_user');


// Register end point
router.post('/register', function(req, res, next){
    if (req.body.password !== req.body.passwordConf){
        var err = new Error('Password do not match.');
        err.status = 400;
        res.send("password dont match");
        return next(err);
    }

    if (
        req.body.email && req.body.username && req.body.password &&
        req.body.passwordConf && req.body.gender && req.body.hospital &&
        req.body.condition
    ){
        var patientData = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
            passwordConf: req.body.passwordConf,
            gender: req.body.gender,
            hospital: req.body.hospital,
            condition: req.body.condition
        }
        PatientUser.create(patientData, function(error, patientUser){
            if (error){
                return next(error);
            }
            else {
                req.session.userId = patientUser._id;
                res.send("Patient successfully created");
            }
        })
    }
    else {
        var err = new Error('All fields required for registration');
        err.status = 400;
        return next(err);
    }
})

// Login end point
router.post('/login', function(req, res, next){
    if (req.body.logemail && req.body.logpassword) {
        PatientUser.authenticate(req.body.logemail, req.body.logpassword, function(error, patientUser) {
            if (error || !patientUser){
                var err = new Error('Wrong email or password');
                err.status = 401;
                return next(err);
            } else {
                req.session.userId = patientUser._id;
                res.send("Patient successfully login");
            }
        })
    }
    else {
        var err = new Error('Email and password required for login');
        err.status = 400;
        return next(err);
    }
})

// Logout end point
router.get('/logout', function(req, res, next){
    if (req.session){
        req.session.destroy(function(err) {
            if (err) {
                return next(err);
            }
            else {
                res.send("Successfully logout.")
            }
        })
    }
})

// Delete end point
router.delete('/delete', function(req, res){
    PatientUser.remove({
        email: req.body.email
    }, function(err, patientUser){});
    res.send('Deleting is complete for ' + req.body.email);
});

module.exports = router
