var express = require('express');
var router = express.Router();
var DrawingActivity = require('../models/drawing_activity');

// Get Drawing Activity by id
router.get('/:drawingActivityId', function(req, res){
    drawingActivityId = req.params.drawingActivityId;

    DrawingActivity.find({ drawingActivityId : drawingActivityId}, function(err, drawingActivity) {
        res.send("Drawing Activity gotten " + drawingActivity)
    });
});

// Get Drawing Activities by patient email
router.get('/email/:patientEmail', function(req, res) {
    patientEmail = req.params.patientEmail;

    DrawingActivity.find({ patientEmail: patientEmail}, function(err, drawingActivities) {
        res.send("Drawing Activities gotten " + drawingActivities);
    })
})

// Create new Drawing Activity data
router.post('/', function(req, res){
    if (req.body.drawingActivityId && req.body.patientEmail && req.body.timestamp && req.body.image){
        var newDrawingActivity = new DrawingActivity(req.body);
        newDrawingActivity.save(function(err, newDrawingActivity){
            if (err) return console.error(err);
            res.send('Drawing Activity has been submitted for the day ' + newDrawingActivity);
        })
    }
});

// Delete Drawing Activity data
router.delete('/:drawingActivityId', function(req, res){
    DrawingActivity.deleteOne({
        drawingActivityId: req.params.drawingActivityId
    }, function(err, drawingActivity){});
    res.send('Deleting is complete for ' + req.params.drawingActivityId);
});

module.exports = router
