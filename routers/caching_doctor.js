var express = require('express');
var router = express.Router();
var CachingDoctor = require('../models/caching_doctor');

// Get Caching Doctor by id
router.get('/:cachingDoctorId', function(req, res){
    cachingDoctorId = req.params.cachingDoctorId;

    CachingDoctor.find({ cachingDoctorId : cachingDoctorId}, function(err, cachingDoctor) {
        res.send("Caching Doctor gotten " + cachingDoctor)
    });
});

// Get Caching Doctors by doctor's email
router.get('/email/:doctorEmail', function(req, res) {
    doctorEmail = req.params.doctorEmail;

    CachingDoctor.find({ doctorEmail: doctorEmail}, function(err, cachingDoctors) {
        console.log(cachingDoctors);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(cachingDoctors));
    })
})


// Create new Caching Doctor data
router.post('/', function(req, res){
	if (req.body.cachingDoctorId && req.body.doctorEmail && req.body.patientEmail && 
		req.body.timestamp && req.body.activityName)
	{
        var newCachingDoctor = new CachingDoctor(req.body);
        newCachingDoctor.save(function(err, newCachingDoctor){
            if (err) return console.error(err);
            res.send('Caching Doctor has been submitted for the day ' + newCachingDoctor);
        })
    }
});

// Delete Caching Doctor data
router.delete('/:cachingDoctorId', function(req, res){
    CachingDoctor.deleteOne({
        cachingDoctorId: req.params.cachingDoctorId
    }, function(err, cachingDoctor){});
    res.send('Deleting is complete for ' + req.params.cachingDoctorId);
});

module.exports = router
