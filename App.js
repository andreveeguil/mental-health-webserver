var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

var mongodbConnection = 'mongodb://andreveeguil:DGM567iion@ds123728.mlab.com:23728/mental_health';
mongoose.connect(mongodbConnection);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});

app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var doctorUserRoute = require('./routers/doctor_user');
var patientUserRoute = require('./routers/patient_user');
var dailyEmotionRoute = require('./routers/daily_emotion');
var colourActivityRoute = require('./routers/color_activity');
var pictureActivityRoute = require('./routers/picture_activity');
var drawingActivityRoute = require('./routers/drawing_activity');
var questionnaireQuestionRoute = require('./routers/questionnaire_question');
var questionnaireAnswerRoute = require('./routers/questionnaire_answer');
var shapeActivityRoute = require('./routers/shape_activity');
var doctorSubscription = require('./routers/doctor_subscription');
var cachingDoctor = require('./routers/caching_doctor');
var cachingPatient = require('./routers/caching_patient');

app.use('/doctor', doctorUserRoute);
app.use('/patient', patientUserRoute);
app.use('/dailyEmotion', dailyEmotionRoute);
app.use('/colourActivity', colourActivityRoute);
app.use('/pictureActivity', pictureActivityRoute);
app.use('/drawingActivity', drawingActivityRoute);
app.use('/questionnaireQuestion', questionnaireQuestionRoute);
app.use('/questionnaireAnswer', questionnaireAnswerRoute);
app.use('/shapeActivity', shapeActivityRoute);
app.use('/doctorSubscription', doctorSubscription);
app.use('/cachingDoctor', cachingDoctor);
app.use('/cachingPatient', cachingPatient);

app.use(function(req, res, next) {
    var err = new Error('API not found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send(err.message);
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))
