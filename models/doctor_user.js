var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var DoctorUserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    passwordConf: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    hospital: {
        type: String,
        required: true
    },
    personalComment: {
        type: String
    }
});

DoctorUserSchema.pre('save', function(next){
    var doctorUser = this;
    bcrypt.hash(doctorUser.password, 10, function(err, hash) {
        if (err) {
            return next(err);
        }
        doctorUser.password = hash;
        next();
    })
})

DoctorUserSchema.statics.authenticate = function(email, password, callback){
    DoctorUser.findOne({ email: email })
        .exec(function(err, doctorUser) {
            if (err) {
                return callback(err);
            }
            else if (!doctorUser){
                var err = new Error('Doctor User is not found.');
                err.status = 401;
                return callback(err);
            }
            bcrypt.compare(password, doctorUser.password, function(err, result){
                if (result == true){
                    return callback(null, doctorUser);
                }
                else {
                    return callback();
                }
            })
        })
}

var DoctorUser = mongoose.model('DoctorUser', DoctorUserSchema);

module.exports = DoctorUser;
