var mongoose = require('mongoose');

var ShapeActivitySchema = new mongoose.Schema({
    shapeActivityId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    image: {
        type: Buffer,
        required: true
    }
});

var ShapeActivity = mongoose.model('ShapeActivity', ShapeActivitySchema);
module.exports = ShapeActivity;
