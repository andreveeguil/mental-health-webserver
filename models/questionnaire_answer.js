var mongoose = require('mongoose');

var QuestionnaireAnswerSchema = new mongoose.Schema({
    questionnaireAnswerId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    questionnaireId: {
        type: String,
        required: true,
        trim: true,
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    answers: {
        type: Array
    }
});

var QuestionnaireAnswer = mongoose.model('QuestionnaireAnswer', QuestionnaireAnswerSchema);
module.exports = QuestionnaireAnswer;
