var mongoose = require('mongoose');

var CachingPatientSchema = new mongoose.Schema({
    cachingPatientId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
	},
    timestamp: {
        type: Date,
        required: true
	},
	activityName: {
		type: String,
		required: true
	}
});

var CachingPatient = mongoose.model('CachingPatient', CachingPatientSchema);
module.exports = CachingPatient;
