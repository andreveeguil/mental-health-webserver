var mongoose = require('mongoose');

var DoctorSubscriptionSchema = new mongoose.Schema({
    doctorSubscriptionId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    doctorEmail: {
        type: String,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
});

var DoctorSubscription = mongoose.model('DoctorSubscription', DoctorSubscriptionSchema);
module.exports = DoctorSubscription;
