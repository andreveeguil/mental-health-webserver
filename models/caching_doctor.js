var mongoose = require('mongoose');

var CachingDoctorSchema = new mongoose.Schema({
    cachingDoctorId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    doctorEmail: {
        type: String,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    activityName: {
        type: String,
        required: true
    }
});

var CachingDoctor = mongoose.model('CachingDoctor', CachingDoctorSchema);
module.exports = CachingDoctor;
