var mongoose = require('mongoose');

var PictureActivitySchema = new mongoose.Schema({
    pictureActivityId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    image: {
        type: Buffer,
        required: true
    }
});

var PictureActivity = mongoose.model('PictureActivity', PictureActivitySchema);
module.exports = PictureActivity;
