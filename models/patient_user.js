var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var PatientUserSchema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    password: {
        type: String,
        required: true
    },
    passwordConf: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    hospital: {
        type: String,
        required: true
    },
    condition: {
        type: String,
        required: true
    }
});

PatientUserSchema.pre('save', function(next){
    var patientUser = this;
    bcrypt.hash(patientUser.password, 10, function(err, hash) {
        if (err) {
            return next(err);
        }
        patientUser.password = hash;
        next();
    })
})

PatientUserSchema.statics.authenticate = function(email, password, callback){
    PatientUser.findOne({ email: email })
        .exec(function(err, patientUser) {
            if (err) {
                return callback(err);
            }
            else if (!patientUser){
                var err = new Error('Patient User is not found.');
                err.status = 401;
                return callback(err);
            }
            bcrypt.compare(password, patientUser.password, function(err, result){
                if (result == true){
                    return callback(null, patientUser);
                }
                else {
                    return callback();
                }
            })
        })
}

var PatientUser = mongoose.model('PatientUser', PatientUserSchema);
module.exports = PatientUser;
