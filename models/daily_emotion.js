var mongoose = require('mongoose');

var DailyEmotionSchema = new mongoose.Schema({
    dailyEmotionId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    emotion: {
        type: String,
        required: true,
    }
});

var DailyEmotion = mongoose.model('DailyEmotion', DailyEmotionSchema);
module.exports = DailyEmotion;
