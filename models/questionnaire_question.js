var mongoose = require('mongoose');

var QuestionnaireSchema = new mongoose.Schema({
    questionnaireQuestionId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    questionnaireId: {
        type: String,
        required: true,
        trim: true
    },
    questionnaireName: {
        type: String,
        trim: true
    },
    question: {
        type: String,
        required: true
    },
    questionIndex: {
        type: Number,
        required: true
    },
    format: {
        type: String,
        required: true
    },
    firstChoice: {
        type: String, 
    },
    secondChoice: {
        type: String, 
    },
    thirdChoice: {
        type: String, 
    },
    fourthChoice: {
        type: String, 
    },
    fifthChoice: {
        type: String, 
    },
    textAns: {
        type: String, 
    }
});

var Questionnaire = mongoose.model('Questionnaire', QuestionnaireSchema);
module.exports = Questionnaire;
