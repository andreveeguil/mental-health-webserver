var mongoose = require('mongoose');

var DrawingActivitySchema = new mongoose.Schema({
    drawingActivityId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    image: {
        type: Buffer,
        required: true
    }
});

var DrawingActivity = mongoose.model('DrawingActivity', DrawingActivitySchema);
module.exports = DrawingActivity;
