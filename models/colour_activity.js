var mongoose = require('mongoose');

var ColourActivitySchema = new mongoose.Schema({
    colourActivityId: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    patientEmail: {
        type: String,
        required: true,
        trim: true
    },
    timestamp: {
        type: Date,
        required: true
    },
    colourChoice: {
        type: String,
        required: true
    }
});

var ColourActivity = mongoose.model('ColourActivity', ColourActivitySchema);
module.exports = ColourActivity;
